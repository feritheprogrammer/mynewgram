@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/p" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="row justify-content-center pb-3">
                        <h1>Add New Post</h1>
                    </div>

                    <div class="form-group row">
                        <label for="caption" class="col-4 col-form-label">Post Caption</label>
                        <div class="col-8">
                            <input id="caption" type="text"
                                   class="form-control @error('caption') is-invalid @enderror"
                                   name="caption"
                                   value="{{ old('caption') }}"
                                   autocomplete="caption"
                                   autofocus>

                            @error('caption')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="image" class="col-4 col-form-label">Post Image</label>
                        <div class="col-8">
                            <input id="image" type="file"
                                   class="form-control @error('caption') is-invalid @enderror"
                                   name="image">

                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row form-group ">
                        <button class="btn btn-primary ml-3">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
