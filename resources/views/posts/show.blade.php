@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8">
                <img src="{{$post->image}}" class="w-100">
            </div>
            <div class="col-4">
                <div class="d-flex align-items-baseline">
                    <div>
                        <img src="{{ $post->user->profile->image }}"
                             style="max-height: 40px"
                             class="rounded-circle mr-2">
                    </div>
                    <div class="d-flex">
                        <h5><a href="/u/{{ $post->user->id }}">
                                <span class="text-dark">{{ $post->user->username }}</span>
                            </a></h5>
                        <a class="pl-2" href="#">follow</a>
                    </div>
                </div>
                <hr>
                <p>
                    {{$post->caption}}
                </p>
            </div>
        </div>
    </div>
@endsection
