<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => User::pluck('id')->random(1)->first(),
        'caption' => $faker->text,
        'image' => $faker->imageUrl(400, 200, 'sports'),
    ];
});
