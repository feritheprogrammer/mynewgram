<?php

use App\Profile;
use App\User;
use App\Post;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 20)->create()
            ->each(function($user){
                $user->profile()->update((factory(Profile::class)->make())->toArray());
                $user->posts()->saveMany(factory(Post::class, rand(2,8))->make());
            });

        $users = User::all();
        foreach($users as $user){
            $following = User::all()->random(rand(2,15));
            $user->following()->attach($following);
        }
    }
}
