<?php

Route::view('/', 'welcome');

Auth::routes();

Route::get('/u/{user}', 'ProfilesController@index');
Route::get('/u/{user}/edit', 'ProfilesController@edit');
Route::patch('/u/{user}', 'ProfilesController@update');

Route::post('/follow/{user}', 'FollowsController@store');


Route::get('/p', 'PostsController@index');
Route::get('/p/create','PostsController@create');
Route::get('/p/{post}','PostsController@show');
Route::post('/p', 'PostsController@store');

Route::get('/home', 'HomeController@index')->name('home');
