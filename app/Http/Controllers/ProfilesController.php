<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    public function index(User $user)
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;

        $postCount = Cache::remember(
            'count.posts' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return $user->posts->count();
            });

        $followersCount = Cache::remember(
            'count.followers.' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return $user->profile->followers->count();
            });

        $followingCount = Cache::remember(
            'count.following.' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return $user->following->count();
            });

        return view('profiles.index', compact([
            'user',
            'follows',
            'postCount',
            'followersCount',
            'followingCount',
        ]));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user->profile);
        return view('profiles.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user->profile);

        $data = $request->validate([
            'title' => 'required|min:3',
            'description' => 'min:3',
            'url' => 'url',
        ]);

        if ($request['image']) {
            $request->validate([
                'image' => 'image',
            ]);
            $imagePath = $request['image']->store('profile', 'public');
            $imagePath = "/storage/" . $imagePath;
            $image = Image::make(public_path("{$imagePath}"))->fit(1000);
            $image->save();

            $data = array_merge($data, ['image' => $imagePath]);
        }

        auth()->user()->profile->update($data);

        return redirect("/u/{$user->id}");
    }
}
