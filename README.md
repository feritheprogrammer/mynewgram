# dragonPlace

I am making my own version of **Instagram** using _Laravel_ & _Vue.js_ and named it dragonPlace (I don't know why).


## How to run?

1. Make sure git and composer are installed.
2. Clone the repository.
3. Open cmd (or terminal), change directory to project folder, and run `composer update`
4. Enter `php artisan key:generate`.
5. Initialize parameters of .env file.
6. Enter `php artisan migrate --seed`
7. Serve the project, open the browser, and go to http://127.0.0.1:8000/


## Required packages

* Intervention.io

## Next steps of the project
* Design a better style
* Add Like and Comment for posts
* Maybe replace Vue.js with react
